import React, { Component } from 'react';
import { createAppContainer } from 'react-navigation';
import MainStack from './navigation/MainStack';
import { Provider } from 'react-redux';
import store from './redux/store';

const AppContainer = createAppContainer(MainStack);

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <AppContainer />
      </Provider>
    );
  }
}

export default App;
