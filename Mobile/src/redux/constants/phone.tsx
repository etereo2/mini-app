/*
 * constants
 */
//Initiate the api call
export const FETCH_DATA = 'FETCH_DATA';
//Gets the phones on api call is fullfilled
export const GET_PHONES_FULFILLED = 'GET_PHONES_FULFILLED';
//When there is a error return an errror action type.
export const FETCH_REJECTED = 'GET_PHONES_REJECTED';

export const SEARCH_PHONE_FULLFILLED = 'SEARCH_PHONE_FULLFILLED';
