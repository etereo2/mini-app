import {
    fetchData,
    fetchDataFulfilled,
    fetchDataRejected,
    searchFulfilled
} from './actions/phone';
import { BACKEND_URL } from '../Constants';
import axios from 'axios';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../redux/reducers';
import { PhoneActionType } from '../types';

/*
 * Summary: Get all phones on server.
 *
 * Description: Middleware operation to get phones with axios and dispatching loader behaviour and results.
 *
 * @param {page} target page at the server.
 */
export const getPhones = (page: number) => {
    return (dispatch: (arg0: PhoneActionType) => void) => {
        dispatch(fetchData(true));
        axios
            .get(`${BACKEND_URL}/phones?pageSize=2&page=${page}`)
            .then((res) => {
                if (res.data.status === 'ok') {
                    dispatch(fetchDataFulfilled(res.data));
                }
            })
            .catch((err) => {
                console.log('Error', err);
                dispatch(fetchDataRejected(err));
            });
    };
};

/*
 * Summary: Searches a phone from the server.
 *
 * Description: Middleware operation to search a phones with axios and dispatching loader behaviour and search result.
 *
 * @param {text} string to search at the server.
 */
export const searchPhone = (text: string) => {
    return (dispatch: (arg0: PhoneActionType) => void) => {
        dispatch(fetchData(true));
        axios
            .get(`${BACKEND_URL}/phones?search=${text}`)
            .then((res) => {
                if (res.data.status === 'ok') {
                    dispatch(searchFulfilled({ phones: [res.data.phone] }));
                }
            })
            .catch((err) => {
                console.log('Error', err);
                dispatch(fetchDataRejected(err.message));
            });
    };
};

export default createStore(rootReducer, applyMiddleware(thunk));
