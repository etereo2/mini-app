import {
    FETCH_DATA,
    SEARCH_PHONE_FULLFILLED,
    GET_PHONES_FULFILLED,
    FETCH_REJECTED
} from '../constants/phone';
import { PayloadType } from '../../types';

export const fetchData = (bool: boolean) => {
    return {
        type: FETCH_DATA,
        loading: bool
    };
};

export const fetchDataFulfilled = (data: PayloadType) => {
    return {
        type: GET_PHONES_FULFILLED,
        payload: data,
        loading: false
    };
};

export const searchFulfilled = (data: PayloadType) => {
    return {
        type: SEARCH_PHONE_FULLFILLED,
        payload: data,
        loading: false
    };
};

export const fetchDataRejected = (message: string) => {
    return {
        type: FETCH_REJECTED,
        payload: { error: { message } },
        loading: false
    };
};
