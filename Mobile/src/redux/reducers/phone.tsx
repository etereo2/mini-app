import { PhoneActionType, PhoneType } from '../../types';
import {
    FETCH_DATA,
    GET_PHONES_FULFILLED,
    FETCH_REJECTED,
    SEARCH_PHONE_FULLFILLED
} from '../constants/phone';

const initialState = {
    // Phones array.
    list: [],
    // Server indicator if there are more phones to load.
    hasMore: false,
    // Local indicator if it's done getting data.
    loading: true,
    // Error message in case of unexpected behaviours.
    errorMessage: '',
    // TargetPage to query from server.
    targetPage: 1
};

const reducer = (state = initialState, action: PhoneActionType) => {
    switch (action.type) {
        case FETCH_DATA:
            return { ...state, loading: action.loading };
        case GET_PHONES_FULFILLED:
            let targetPage = state.targetPage;
            let list: Array<PhoneType> | undefined = [];
            if (state.targetPage === 1) {
                // Cleaning in case there was a search result.
                list = action?.payload?.phones;
            } else if (action?.payload?.phones) {
                // Deep clone of state.list and generation of completely new array.
                list = [
                    ...JSON.parse(JSON.stringify(state.list)),
                    ...action.payload.phones
                ];
            }
            if (action?.payload?.hasMore) {
                targetPage = state.targetPage + 1;
            }
            return {
                ...state,
                list,
                hasMore: action?.payload?.hasMore,
                loading: action.loading,
                targetPage
            };
        case FETCH_REJECTED:
            return {
                ...state,
                errorMessage: action?.payload?.error?.message,
                loading: false,
                hasMore: false,
                targetPage: 1
            };
        case SEARCH_PHONE_FULLFILLED:
            return {
                ...state,
                list: action?.payload?.phones,
                loading: false,
                hasMore: false,
                targetPage: 1
            };
        default:
            return state;
    }
};

export default reducer;
