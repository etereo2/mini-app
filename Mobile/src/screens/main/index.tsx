import React from 'react';
import {
    SafeAreaView,
    Image,
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    TextInput,
    Button
} from 'react-native';
import { MainType, PhoneReducerType } from '../../types';
import PhoneListContainer from '../../components/PhonesListContainer';
import { connect } from 'react-redux';
import Loader from '../../components/Loader';

/**
 * Summary: Display of all phones.
 *
 * Description: This component encapsulates the logic to display all the phones incomming from the server.
 * It is composed by a header, search bar and PhoneListContainer.
 *
 * @file This files defines the Main function component.
 * @author Rafael Etcheverry.
 */
function Main({ loading, navigation }: MainType) {
    const [text, setText] = React.useState('');
    const [searchPhone, setSearchPhone] = React.useState(false);
    const [visibleSearch, setVisibleSearch] = React.useState(false);

    return (
        <SafeAreaView style={styles.content}>
            <View>
                <View style={styles.innerHeader}>
                    <TouchableOpacity
                        onPress={() => setVisibleSearch(!visibleSearch)}>
                        <Image
                            style={styles.maginfyingGlass}
                            source={require('../../assets/magnifyingGlass.png')}
                        />
                    </TouchableOpacity>
                    <Text style={styles.heading}>Phones</Text>
                    {visibleSearch && (
                        <TouchableOpacity
                            style={styles.closeSearch}
                            onPress={() => {
                                setVisibleSearch(!visibleSearch);
                                setSearchPhone(false);
                                setText('');
                            }}>
                            <Text style={styles.closeText}> X </Text>
                        </TouchableOpacity>
                    )}
                    {!visibleSearch && <View style={styles.closeSearch} />}
                </View>
                {visibleSearch ? (
                    <>
                        <TextInput
                            autoFocus={true}
                            style={styles.searchInput}
                            onChangeText={(value: string) => {
                                if (searchPhone) {
                                    setSearchPhone(false);
                                }
                                setText(value);
                            }}
                            value={text}
                            autoCapitalize="none"
                        />
                        <View style={styles.searchBtn}>
                            <Button
                                title={'Search'}
                                onPress={() => setSearchPhone(true)}
                            />
                        </View>
                    </>
                ) : null}
            </View>
            <PhoneListContainer
                navigation={navigation}
                searchText={searchPhone ? text : false}
            />
            {loading ? <Loader visible={loading} /> : null}
        </SafeAreaView>
    );
}

const mapStateToProps = ({ phone }: { phone: PhoneReducerType }) => ({
    loading: phone.loading
});

export default connect(mapStateToProps)(Main);

const styles = StyleSheet.create({
    content: {
        flex: 1
    },
    innerHeader: {
        flexDirection: 'row'
    },
    heading: {
        width: '100%',
        textAlign: 'center',
        fontSize: 25,
        marginTop: 10,
        flex: 1
    },
    maginfyingGlass: {
        width: 30,
        height: 30,
        marginTop: 8,
        marginLeft: 8
    },
    closeSearch: {
        width: 30,
        height: 30,
        marginTop: 8,
        marginRight: 8
    },
    closeText: {
        fontSize: 25
    },
    searchInput: {
        marginTop: 10,
        marginHorizontal: 10,
        borderColor: 'grey',
        borderWidth: 1,
        backgroundColor: 'white',
        height: 30,
        padding: 5
    },
    searchBtn: {
        marginHorizontal: 10,
        marginVertical: 5
    }
});
