import React from 'react';
import PhoneDetailsComponent from '../../components/PhoneDetailsComponent';
import { SafeAreaView } from 'react-navigation';
import { DetailsType } from '../../types';

/**
 * Summary: Display of phone details.
 *
 * Description: This component encapsulates the logic to display of the details of a phone.
 * It is composed by a basic container and inside it displays the PhoneDetailsComponent
 * with it's specific phone info.
 *
 * @file This files defines the Details function component.
 * @author Rafael Etcheverry.
 */
function Details({ navigation }: DetailsType) {
    const phone = navigation.getParam('phone');
    return (
        <SafeAreaView>
            <PhoneDetailsComponent phone={phone} />
        </SafeAreaView>
    );
}

export default Details;
