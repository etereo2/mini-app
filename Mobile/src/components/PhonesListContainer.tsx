import React, { useEffect } from 'react';
import { FlatList, Image, Text, View, Button, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { getPhones, searchPhone } from '../redux/store';
import { PhoneListContainerType, PhoneType, PhoneReducerType } from '../types';

/**
 * Summary: Phones list.
 *
 * Description: This component encapsulates the logic to display only the phones.
 * It is responsible for refreshing the phones list from the server.
 * It's encapsulation makes it reusable for future uses of this list.
 *
 * @file This files defines the Main function component.
 * @author Rafael Etcheverry.
 */
const PhoneListContainer = ({
    searchText,
    phones,
    hasMore,
    targetPage,
    navigation,
    onGetPhones,
    onSearchPhone
}: PhoneListContainerType) => {
    useEffect(() => {
        if (searchText) {
            onSearchPhone(searchText);
        } else {
            onGetPhones(targetPage);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [searchText]);

    const footer = () => {
        if (hasMore && !searchText) {
            return (
                <View style={styles.loadMore}>
                    <Button
                        title="loadMore"
                        onPress={() => onGetPhones(targetPage)}
                    />
                </View>
            );
        }
        return <View style={styles.footer} />;
    };

    return (
        <FlatList
            keyExtractor={(item, index) => String(index)}
            style={styles.phonesList}
            data={phones}
            ListFooterComponent={footer()}
            renderItem={({ item }: { item: PhoneType }) => (
                <View style={styles.item}>
                    <Image
                        style={styles.image}
                        source={{ uri: item.image }}
                        resizeMode={'contain'}
                    />
                    <View style={styles.rightContainer}>
                        <Text style={styles.title}>{item.title}</Text>
                        <Text style={styles.price}>Precio: {item.price}</Text>
                        <View style={styles.details}>
                            <Button
                                title="Details"
                                onPress={() =>
                                    navigation.push('Details', { phone: item })
                                }
                            />
                        </View>
                    </View>
                </View>
            )}
        />
    );
};

const styles = StyleSheet.create({
    phonesList: {
        marginTop: 10
    },
    item: {
        flexDirection: 'row',
        marginVertical: 5,
        marginHorizontal: 10,
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: 'black',
        borderRadius: 5
    },
    rightContainer: {
        flex: 1,
        justifyContent: 'center',
        marginRight: 5
    },
    image: {
        width: 100,
        height: 100,
        marginVertical: 10
    },
    title: {
        fontSize: 20,
        textAlign: 'center'
    },
    price: {
        textAlign: 'center',
        marginTop: 5
    },
    details: {
        marginTop: 10,
        marginBottom: 10
    },
    loadMore: {
        marginHorizontal: 10,
        marginTop: 5,
        marginBottom: 15
    },
    footer: {
        height: 15
    }
});

const mapStateToProps = ({ phone }: { phone: PhoneReducerType }) => ({
    phones: phone.list,
    loading: phone.loading,
    hasMore: phone.hasMore,
    targetPage: phone.targetPage
});

const mapDispatchToProps = {
    onGetPhones: getPhones,
    onSearchPhone: searchPhone
};

export default connect(mapStateToProps, mapDispatchToProps)(PhoneListContainer);
