import React from 'react';
import { Text } from 'react-native';
import { shallow } from 'enzyme';
import PhoneDetailsComponent from './PhoneDetailsComponent';

describe('Phone Details Component Tests', () => {
    const phone = {
        id: 'xiaomi-redmi-9c',
        title: 'Xiaomi Redmi 9C',
        image:
            'https://images-na.ssl-images-amazon.com/images/I/61fnL6uGq1L._AC_SL1000_.jpg',
        description:
            'AI Rear Triple Camera, Make memories last with the AI triple camera, capturing your favorite moments in vivid color.13MP main camera, 2MP depth sensor, LED flasher, 2MP macro camera.',
        price: '139,00 €'
    };

    const wrapper = shallow(<PhoneDetailsComponent phone={phone} />);

    it('Total Text-views in PhoneDetailsComponent is OK', () => {
        expect(wrapper.find(Text).length).toBe(3);
    });

    it('Title value is OK', () => {
        expect(wrapper.find(Text).at(0).props().children).toEqual(
            'Xiaomi Redmi 9C'
        );
    });

    it('Price value is OK', () => {
        expect(wrapper.find(Text).at(1).props().children[1]).toEqual(
            '139,00 €'
        );
    });

    it('Description value is OK', () => {
        expect(wrapper.find(Text).at(2).props().children).toEqual(
            'AI Rear Triple Camera, Make memories last with the AI triple camera, capturing your favorite moments in vivid color.13MP main camera, 2MP depth sensor, LED flasher, 2MP macro camera.'
        );
    });
});
