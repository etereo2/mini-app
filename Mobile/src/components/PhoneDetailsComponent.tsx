import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import { PhoneDetailsTypes } from '../types';

/**
 * Summary: Phones details.
 *
 * Description: This component encapsulates the logic to display the phone details.
 * These details are: Title, price, image and description.
 *
 * @file This files defines the PhonesDetails function component.
 * @author Rafael Etcheverry.
 */
function PhoneDetailsComponent({ phone }: PhoneDetailsTypes) {
    return (
        <View style={styles.phone}>
            <Text style={styles.title}>{phone.title}</Text>
            <Text style={styles.price}>Precio: {phone.price}</Text>
            <Image
                style={styles.image}
                source={{ uri: phone.image }}
                resizeMode={'contain'}
            />
            <Text style={styles.description}>{phone.description}</Text>
        </View>
    );
}

export default PhoneDetailsComponent;

const styles = StyleSheet.create({
    content: {
        flex: 1
    },
    phone: {
        marginVertical: 5,
        marginHorizontal: 10,
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: 'black',
        borderRadius: 5
    },
    image: {
        width: 200,
        height: 200,
        marginVertical: 20,
        alignSelf: 'center'
    },
    title: {
        fontSize: 22,
        textAlign: 'center',
        marginTop: 10
    },
    price: {
        textAlign: 'center',
        marginTop: 10
    },
    description: {
        marginVertical: 15,
        marginHorizontal: 15
    }
});
