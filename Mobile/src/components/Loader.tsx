import React from 'react';
import { View, ActivityIndicator, StyleSheet } from 'react-native';

function Loader({ visible }: { visible: boolean }) {
    return visible ? (
        <View style={style.loaderContainer}>
            <View style={style.opacityContainer} />
            <View style={style.indicatorContainer}>
                <ActivityIndicator size="large" color="grey" />
            </View>
        </View>
    ) : null;
}

export default Loader;

const style = StyleSheet.create({
    loaderContainer: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        justifyContent: 'center'
    },
    opacityContainer: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        backgroundColor: 'black',
        opacity: 0.5
    },
    indicatorContainer: {
        borderWidth: 1,
        width: 40,
        backgroundColor: 'white',
        alignSelf: 'center'
    }
});
