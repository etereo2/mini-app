import { createStackNavigator } from 'react-navigation-stack';
import Main from "../screens/main/index";
import Details from "../screens/details/index";

const MainStack = createStackNavigator(
    {
        Main: { screen: Main },
        Details: { screen: Details },
    },
    {
        initialRouteName: 'Main',
        defaultNavigationOptions: {
            headerTitleAlign: 'center'
        }
    }
);

export default MainStack;
