import { StackNavigationProp } from '@react-navigation/stack';

type RootStackParamList = {
    Main: undefined;
    Details: { userId: string };
};

export type MainScreenNavigationProp = StackNavigationProp<
    RootStackParamList,
    'Main'
>;

export interface MainType {
    loading: boolean;
    visible: boolean;
    navigation: MainScreenNavigationProp;
}

export type DetailsScreenNavigationProp = StackNavigationProp<
    RootStackParamList,
    'Details'
>;

export interface DetailsType {
    navigation: DetailsScreenNavigationProp;
}

export interface PhoneType {
    title: string;
    image: string;
    description: string;
    price: string;
}

export interface PhoneDetailsTypes {
    phone: PhoneType;
}

export interface PhoneListContainerType {
    searchText: string | boolean;
    phones: Array<PhoneType>;
    hasMore: boolean;
    targetPage: any;
    navigation: MainScreenNavigationProp;
    onGetPhones: Function;
    onSearchPhone: Function;
}

export interface PayloadType {
    phones?: Array<PhoneType>;
    hasMore?: boolean;
    error?: {
        message?: string;
    };
}

export interface PhoneActionType {
    type: string;
    payload?: PayloadType;
    loading: boolean;
}

export interface PhoneReducerType {
    list: Array<PhoneType>;
    loading: boolean;
    hasMore: boolean;
    targetPage: boolean;
}
