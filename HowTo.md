HOW TO

SERVER

1- De la carpeta raix ir a la carpeta 'Server' con el comando 'cd Server'.
2- Ejecutar npm install para instalar las dependencias del servidor.
3- Ejectuar node build/index para desplegar el servidor.

MOBILE APP
Pre-condición: Desplegar el servidor.

1- Actualizar el archivo Mobile/src/Constants.js con la ip local de tu maquina. 
Ejemplo: IP = '192.168.1.135';
2- De la raiz ir a la carpeta 'Mobile' con el comando 'cd Mobile'.
3- Ejecutar el comando 'npm install' para instalar las dependencias de la app.
4- Ejecutar 'react-native run-android' o 'react-native run-ios' para correrlo en el sistema operativo de su preferencia.

TESTING MOBILE APP

1- De la raiz ir a la carpeta 'Mobile' con el comando 'cd Mobile'.
2- Ejecutar el comando 'nmp test' para ejecutar las pruebas.
