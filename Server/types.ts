export interface Phone {
    id: string,
    description: string,
    price: string,
    image: string
};