import express from 'express';
import { phones } from './phones';
const app = express();
import { Phone } from './types';

function paginate(array: Array<Phone>, page_size: number, page_number: number) {
    return array.slice((page_number - 1) * page_size, page_number * page_size);
}

app.get('/phones', function (req, res) {
    try {
        if (req.query.search) {
            const phone = phones.find((elem) => { return elem.id === req.query.search });
            if (phone) {
                return res.json({ status: 'ok', phone });
            }
            else {
                return res.json({ status: 'ok', message: `There is no match for :${req.query.search}` });
            }
        }
        else if (req.query.page && req.query.pageSize) {
            if (typeof req.query.pageSize === 'string' && typeof req.query.page === 'string') {
                const pageSize = parseInt(req.query.pageSize);
                const page = parseInt(req.query.page);
                const phonesPage = paginate(phones, pageSize, page);
                const hasMore = (phones.length / pageSize) > page;
                return res.json({
                    status: 'ok',
                    phones: phonesPage,
                    hasMore: hasMore
                });
            }
            return res.json({ status: 'error', message: 'Query params missmatch.', });
        }
        return res.json({ status: 'ok', phones, hasMore: false });
    } catch (ex) {
        console.log('ex', ex);
        return res.json({ status: 'error', message: 'An unexpected error has ocurred', ex });
    }
})


app.listen(3001);