"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var phones_1 = require("./phones");
var app = express_1.default();
function paginate(array, page_size, page_number) {
    return array.slice((page_number - 1) * page_size, page_number * page_size);
}
app.get('/phones', function (req, res) {
    try {
        if (req.query.search) {
            var phone = phones_1.phones.find(function (elem) { return elem.id === req.query.search; });
            if (phone) {
                return res.json({ status: 'ok', phone: phone });
            }
            else {
                return res.json({ status: 'ok', message: "There is no match for :" + req.query.search });
            }
        }
        else if (req.query.page && req.query.pageSize) {
            if (typeof req.query.pageSize === 'string' && typeof req.query.page === 'string') {
                var pageSize = parseInt(req.query.pageSize);
                var page = parseInt(req.query.page);
                var phonesPage = paginate(phones_1.phones, pageSize, page);
                var hasMore = (phones_1.phones.length / pageSize) > page;
                return res.json({
                    status: 'ok',
                    phones: phonesPage,
                    hasMore: hasMore
                });
            }
            return res.json({ status: 'error', message: 'Query params missmatch.', });
        }
        return res.json({ status: 'ok', phones: phones_1.phones, hasMore: false });
    }
    catch (ex) {
        console.log('ex', ex);
        return res.json({ status: 'error', message: 'An unexpected error has ocurred', ex: ex });
    }
});
app.listen(3001);
