"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.phones = void 0;
exports.phones = [
    {
        "id": "samsung-galaxy-s20",
        "title": "Samsung Galaxy S20 / S20 Plus.",
        "image": "https://i-cdn.phonearena.com/images/phones/78175-xlarge/Samsung-Galaxy-S20-Plus1.jpg",
        "description": "The Samsung Galaxy S20+ comes with a Snapdragon 865/Exynos 990 chipset, 12GB RAM and 128GB/256GB/512GB of storage, as well as a 4,500mAh battery. Quad-camera setup is on the back of the phone: a 12MP primary sensor, an ultra-wide-angle camera, a telephoto camera, and a Time of Flight sensor. The price of Galaxy S20+ starts at $1200.",
        "price": "1000 €"
    },
    {
        "id": "iphone8",
        "title": "iphone 8",
        "image": "https://i.blogs.es/6ab44a/iphone-8-8-plus/1366_2000.jpg",
        "description": "El Apple iPhone 8 es el modelo más básico de la última generación de smartphones de Apple. Su diseño es unibody con cristal en la parte trasera y tiene un grosor de 7,3 milímetros. Es un móvil ligero para su tamaño y se queda en 148 gramos. El Apple iPhone 8 hace gala de una pantalla de 4,7 pulgadas de diagonal.",
        "price": "400 €"
    },
    {
        "id": "iphone9",
        "title": "iphone 9",
        "image": "https://d1eh9yux7w8iql.cloudfront.net/product_images/36829_0419ea3f-5fa1-418f-8001-dbe3201f3b37.jpg",
        "description": "Pantalla IPS con un tamaño de 4,7 pulgadas y resolución de 1.334 x 750 píxeles. SoC Apple A13 con CPU de seis núcleos, unidad neural para IA y GPU Apple personalizada con cuatro núcleos gráficos. 3 GB de memoria RAM. 64 GB de capacidad de almacenamiento.26 feb. 2020",
        "price": "500 €"
    },
    {
        "id": "iphone11",
        "title": "iphone 11",
        "image": "https://d1eh9yux7w8iql.cloudfront.net/product_images/290057_943d7107-8cd5-470a-9e37-b1feb53d1de6.jpg",
        "description": "Apple asegura que posee la CPU y GPU más rápidas del mercado. Cuenta con 4 GB de memoria y 64 / 128 / 512 GB de almacenamiento interno. iPhone 11 posee dos cámaras traseras. El sensor principal es de 12 MP y 26 mm, con una apertura f/1.8, un tamaño de píxel de 1.4 micras, Autofoco y OIS.",
        "price": "689,00 €"
    },
    {
        "id": "xiaomi-redmi-9c",
        "title": "Xiaomi Redmi 9C",
        "image": "https://images-na.ssl-images-amazon.com/images/I/61fnL6uGq1L._AC_SL1000_.jpg",
        "description": "AI Rear Triple Camera, Make memories last with the AI triple camera, capturing your favorite moments in vivid color.13MP main camera, 2MP depth sensor, LED flasher, 2MP macro camera.",
        "price": "139,00 €"
    }
];
